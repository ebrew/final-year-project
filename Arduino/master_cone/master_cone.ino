#define ENA   14          // Enable/speed motors Right        GPIO14(D5)
#define ENB   12          // Enable/speed motors Left         GPIO12(D6)
#define IN_1  15          // L298N in1 motors Right           GPIO15(D8)
#define IN_2  13          // L298N in2 motors Right           GPIO13(D7)
#define IN_3  2           // L298N in3 motors Left            GPIO2(D4)
#define IN_4  0           // L298N in4 motors Left            GPIO0(D3)

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
ESP8266WebServer server1(80);             // creating a webserver called server1 that communicate on port 80
WiFiServer server(100);                  //  wifi server that helps to communicate to the wifi client in port 100
WiFiClient client;

String check;
String command;                                //String to store app command state.
int speedCar = 800;                          // 400 - 1023.
int speed_Coeff = 3;
int number;

byte ledPin = 2;
const char* ssid = "Master_cone";
const char* password = "123123123";


void setup() {
  pinMode(ENA, OUTPUT);
  pinMode(ENB, OUTPUT);
  pinMode(IN_1, OUTPUT);
  pinMode(IN_2, OUTPUT);
  pinMode(IN_3, OUTPUT);
  pinMode(IN_4, OUTPUT);

IPAddress IP(192, 168, 4, 1);                   // set ip address to 192.168.4.1
IPAddress mask = (255, 255, 255, 0);
 WiFi.softAPConfig(IP, IP, mask);
 
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, password);
  server.begin();
  Serial.begin(9600);

  pinMode(ledPin, OUTPUT);
  Serial.println();
  Serial.println("master cone is running .....");
  Serial.println("Server started.");
  Serial.print("IP: ");     Serial.println(WiFi.softAPIP());
  Serial.print("MAC:");     Serial.println(WiFi.softAPmacAddress());


  //Starting WEB-server
  server1.on ( "/", HTTP_handleRoot );
  server1.onNotFound ( HTTP_handleRoot );
  server1.begin();

}

void loop() {
  server1.handleClient();
  check_input();

  if (number == 1) {
    HTTP_handleRoot();
    action();
    server1.handleClient();
  }

  else if (number == 2) {
    HTTP_handleRoot();
    send_recieve();    // sends and recieve message from the other client .
    server1.handleClient();
  }

  else if (number == 3) {
    HTTP_handleRoot();
    action();
    send_recieve();    // sends and recieve message from the other client .
    server1.handleClient();
  }
}


void check_input() {                    // this function check the inputs of the user to perform instructions.
  HTTP_handleRoot();
  server1.handleClient();

  if (server1.hasArg("Cone")) {
    check = server1.arg("Cone");
    Serial.println(check + "is selected");

    if (check == "C1")
      number = 1;

    else if (check == "C2")
      number = 2;

    else if (check == "C3")
      number = 3;
  }
}

void  goRight() {

  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  analogWrite(ENA, speedCar);

  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);
  analogWrite(ENB, speedCar);
}

void goLeft() {

  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, HIGH);
  analogWrite(ENA, speedCar);

  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, HIGH);
  analogWrite(ENB, speedCar);
}

void goBack() {

  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  analogWrite(ENA, speedCar);

  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, HIGH);
  analogWrite(ENB, speedCar);
}

void goAhead() {

  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, HIGH);
  analogWrite(ENA, speedCar);

  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);
  analogWrite(ENB, speedCar);
}

void goAheadRight() {

  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  analogWrite(ENA, speedCar / speed_Coeff);

  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);
  analogWrite(ENB, speedCar);
}

void goBackRight() {

  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, HIGH);
  analogWrite(ENA, speedCar);

  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, HIGH);
  analogWrite(ENB, speedCar / speed_Coeff);
}

void goBackLeft() {

  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  analogWrite(ENA, speedCar / speed_Coeff);

  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);
  analogWrite(ENB, speedCar);
}

void  goAheadLeft() {

  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  analogWrite(ENA, speedCar);

  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);
  analogWrite(ENB, speedCar / speed_Coeff);
}

void stopRobot() {
  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, LOW);
  analogWrite(ENA, speedCar);

  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, LOW);
  analogWrite(ENB, speedCar);
}

void action()
{ server1.handleClient();
  command = server1.arg("State");
  check = server1.arg("Cone");
  Serial.println(command);

  if      (command == "F") goAhead();
  else if (command == "B") goBack();
  else if (command == "L") goLeft();
  else if (command == "R") goRight();
//  else if (command == "I") goAheadRight();
//  else if (command == "G") goAheadLeft();
//  else if (command == "J") goBackRight();
//  else if (command == "H") goBackLeft();

  else if (command == "0") speedCar = 400;
  else if (command == "1") speedCar = 470;
  else if (command == "2") speedCar = 540;
  else if (command == "3") speedCar = 610;
  else if (command == "4") speedCar = 680;
  else if (command == "5") speedCar = 750;

  else if (command == "6") speedCar = 820;
  else if (command == "7") speedCar = 890;
  else if (command == "8") speedCar = 960;
  else if (command == "9") speedCar = 1023;
  else if (command == "S") stopRobot();
  HTTP_handleRoot();
}

void send_recieve() {

  server1.handleClient();
  HTTP_handleRoot();
  command = server1.arg("State");

  WiFiClient client = server.available();
  if (!client) {
    return;   }
    
  digitalWrite(ledPin, LOW);
  String request = client.readStringUntil('\r');
  
  //Serial.println("From the station: " + request);
  client.flush();

  //Serial.print("Byte sent to the station:  ");
  client.println(command);                       // this function send data to the stationar
  digitalWrite(ledPin, HIGH);

}


void HTTP_handleRoot(void) {
  server1.handleClient();
  if ( server1.hasArg("State") || server1.hasArg("Cone")) {
    //    Serial.println(server1.arg("State"));
    check = server1.arg("Cone");
  }
  server1.send ( 200, "text / html", "" );
  server1.handleClient();
  delay(5);
}
