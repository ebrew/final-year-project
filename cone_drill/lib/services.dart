import 'package:http/http.dart' as http;
import 'dart:convert';
String state;
class Connections{
  final _root = "http://192.168.4.1"; 

  // static Future<ConnectionResponses>getBtton1() async{
  //   String url = "http://192.168.4.1/?Cone=C1";
  //   http.Response response = await http.get("$url");
  //   if (response.statusCode == 200) {
  //     return ConnectionResponses(wifiConnected: true);
  //   } else { 
  //    return ConnectionResponses(wifiConnected: false);
  //   } 
  // }

  getBtton1() async{
    http.Response response = await http.get("$_root/?Cone=C1");
    try{
      print(response.body);
      return ConnectionResponses(wifiConnected: true);
    } catch (e){
      print('Error occured: $e');
      return ConnectionResponses(wifiConnected: false);
    }
  }

  getBtton2() async{
    http.Response response = await http.get("$_root/?Cone=C2");
    return response.body;
  }

  getBtton3() async{
    http.Response response = await http.get("$_root/?Cone=C3");
    return response.body;
  }

  getForeward() async{
    print('Forward called');
    http.Response response = await http.get("$_root/?State=F");
    return response.body;
  }

  getBack() async{
    print('Backward called');
    http.Response response = await http.get("$_root/?State=B");
    return response.body;
  }

  getRight() async{
    print('Right called');
    http.Response response = await http.get("$_root/?State=R");
    return response.body;
  }

  getLeft() async{
    print('Left called');
    http.Response response = await http.get("$_root/?State=L");
    return response.body;
  }

  getStop() async{
    print('Stop called');
    http.Response response = await http.get("$_root/?State=S");
    return response.body;
  }

  // its connectiont to the cloud is placed in the slider widget class (slider.dart)
  // getSlider() async{
  //   http.Response response = await http.get("$_root/?State= _value");
  //   return response.body;
  // }

  getDistace() async{
    http.Response response = await http.get("$_root/?State=D");
    var distance = json.decode(response.body);
    print(distance);
    return distance;
  }
}

class ConnectionResponses{
  bool wifiConnected;

  ConnectionResponses({this.wifiConnected});
}
