import 'package:flutter/material.dart';
import './screens/master_screen.dart';


//Entrypoint
void main() => runApp(App());

class App extends StatelessWidget {
  // This widget is the root of the application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),  
      debugShowCheckedModeBanner: false,
    );
  }
}

