import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Test extends StatefulWidget{
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test>{
  String url = 'http://192.168.4.1/?State=command';

  @override
  void initState() {
    super.initState();
  }

  void showLongToast(){
    Fluttertoast.showToast(
      msg: ('Error 1101: Unable to get a response with the specified URL. Make sure you are connected to the WIFI of the specified URL.'),
      toastLength: Toast.LENGTH_LONG,
      backgroundColor: Colors.white24, 
      textColor: Colors.black,
      gravity: ToastGravity.CENTER
    ); 
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: GestureDetector(
          child: Text('Toast?'),
          onTap: showLongToast
        )
      ),
    );
  }
}