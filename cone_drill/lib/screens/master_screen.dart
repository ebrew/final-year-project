import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'slider.dart';
import '../services.dart';

class Home extends StatefulWidget{
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>{

  String coneStatus = 'None';
  var getDistance = 'Not Set';
  bool toastAlert = false;  //triggers when there is a connection error
  String nothing = '';
  var toastMessage = 'Error 1101: Unable to get a response with the specified URL. Pls make sure you are connected to the WIFI of the specified URL';
  Color active = Colors.lightGreenAccent[400];
  bool activeColor1 = false;
  bool activeColor2 = false;
  bool activeColor3 = false;
  Color inactive = Colors.amberAccent[400];

  void showLongToast(){
    Fluttertoast.showToast(
      msg: (toastMessage),
      toastLength: Toast.LENGTH_LONG,
      backgroundColor: Color(0xfff5f5f5),
      textColor: Colors.black, 
      gravity: ToastGravity.CENTER
    ); 
  }

  // Get the value from the endpoint and prepares it for display
  _getdistance(BuildContext context){
    return FutureBuilder(
      future: Connections().getDistace(),
      builder: (BuildContext context, AsyncSnapshot snapshot){
        List snap = snapshot.data;
        return ListView.builder(
          itemCount: snap.length,
          itemBuilder: (context, index){
            setState(() {
              getDistance = snap[index];
            });
            //return ListTile();
            return getDistance = snap[index];
          }
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(35.0), // here the desired height
          child: AppBar(
            centerTitle: true,
            title: Text('Master Cone App'),
            backgroundColor: Colors.deepOrange[100],
            elevation: 0,
            // iconTheme:  IconThemeData(color: Colors.white),
          )
      ),
      
      body: Stack(
        children: <Widget>[
          // Set the background image 
          Center(
            child: Image.asset(
              'assets/images/bkg.jpg',
              width: size.width,
              height: size.height,
              fit: BoxFit.fill,
            ),
          ),
          _body(context)
        ]
      ),
    ); 
  }
  
  // Body of the app
  _body(BuildContext context){
    // double screenHeight = MediaQuery.of(context).size.height;
    // double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      // height: screenHeight,
      // width: screenWidth,
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.start,
          //   children: <Widget>[
          //     Text(
          //       'Cone Status:',
          //       style: TextStyle(
          //         fontFamily: 'fira',
          //         fontWeight: FontWeight.bold,
          //         color: Colors.black,
          //         fontSize: 25
          //       ),
          //     ),
          //     Container(
          //       color: Colors.lightBlue[100],
          //       width: 130, //105,
          //       height: 35,
          //       margin: EdgeInsets.only(left: 125),
          //       padding: EdgeInsets.only(left: 5, bottom: 3),
          //       child: Text(
          //         coneStatus,
          //         style: TextStyle(
          //           fontFamily: 'fira',
          //           fontStyle: FontStyle.italic,
          //           color: Colors.black,
          //           fontSize: 25,
          //         ),
          //       )
          //     )
          //   ],
          // ),
          // SizedBox(height: 15),  //3
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox(
                height: 72,
                width: 85,
                child: RaisedButton(
                  onPressed: () {
                    Connections().getBtton1();
                    // if(ConnectionResponses(wifiConnected: false)){
                    //   setState(() {
                    //     toastAlert = true;
                    //   });
                    // }
                    toastAlert ? showLongToast(): nothing = ''; // if false
                    setState(() {
                      activeColor1 = true;
                      activeColor2 = false;
                      activeColor3 = false;
                      coneStatus = 'Cone One';
                    });
                  },
                  color: activeColor1? active: inactive,
                  // color: Colors.amberAccent[400],
                  //splashColor: Colors.red,
                  child: Text(
                    "Cone 1",
                    style: TextStyle(fontSize:16, fontWeight: FontWeight.w600, fontFamily: 'fira'),
                  ),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                ),
              ),
              SizedBox(
                height: 72,
                width: 85,
                child: RaisedButton(
                  onPressed: () {
                    Connections().getBtton2();
                    toastAlert ? showLongToast(): nothing = '';
                    setState(() {
                      activeColor2 = true;
                      activeColor1 = false;
                      activeColor3 = false;
                      coneStatus = 'Cone Two';
                    });
                  },
                  color: activeColor2? active: inactive,
                  //splashColor: Colors.red,
                  child: Text(
                    "Cone 2",
                    style: TextStyle(fontSize:16, fontWeight: FontWeight.w600, fontFamily: 'fira'),
                  ),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                ),
              ),
              SizedBox(
                height: 72,
                width: 85,
                child: RaisedButton(
                  onPressed: () {
                    Connections().getBtton3();
                    toastAlert ? showLongToast(): nothing = '';
                    setState(() {
                      activeColor2 = false;
                      activeColor1 = false;
                      activeColor3 = true;
                      coneStatus = 'All Cones';
                    });
                  },
                  color: activeColor3? active: inactive,
                  //splashColor: Colors.red,
                  child: Text(
                    "All Cones",
                    style: TextStyle(fontSize:16, fontWeight: FontWeight.w600, fontFamily: 'fira'),
                  ),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                ),
              ),
            ]
          ),
          SizedBox(height: 15), //20
          Center(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        child: Image.asset("assets/images/u.png", width: 90, height: 90),
                        onTapDown: (TapDownDetails){
                          Connections().getForeward();
                          toastAlert ? showLongToast(): nothing = '';
                        },
                        onTapUp: (TapUpDetails){
                          Connections().getStop();
                        },
                        // splashColor: Colors.red,
                        //color: Colors.white,
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        child: Image.asset("assets/images/l.png", width: 90, height: 90),
                        onTapDown: (TapDownDetails){
                          Connections().getLeft();
                          toastAlert ? showLongToast(): nothing = '';
                        },
                        onTapUp: (TapUpDetails){
                          Connections().getStop();
                        },
                      ),
                      GestureDetector(
                        child: Image.asset("assets/images/s1.png", width: 90, height: 90),
                        onTap: (){
                          Connections().getStop();
                          toastAlert ? showLongToast(): nothing = '';
                        },
                      ),
                      GestureDetector(
                        child: Image.asset("assets/images/r.png", width: 90, height: 90),
                        onTapDown: (TapDownDetails){
                          Connections().getRight();
                          toastAlert ? showLongToast(): nothing = '';
                        },
                        onTapUp: (TapUpDetails){
                          Connections().getStop();
                        },
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        child: Image.asset("assets/images/d.png", width: 90, height: 90),
                        onTapDown: (TapDownDetails){
                          Connections().getBack();
                          toastAlert ? showLongToast(): nothing = '';
                        },
                        onTapUp: (TapUpDetails){
                          Connections().getStop();
                        },
                      )
                    ],
                  ),
                ],
              )
            )
          ),
          SizedBox(height: 5),//15
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text(
                    'Cones',
                    style: TextStyle(
                      fontFamily: 'fira',
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 22
                    ),
                  ),
                  SizedBox(height:15), //1
                  Container(
                    color: Colors.lightBlue[100],
                    width: 128,
                    height: 40,
                    padding: EdgeInsets.only(bottom: 3, top: 5),
                    child: FlatButton(
                      onPressed: (){
                        Connections().getDistace();
                        toastAlert ? showLongToast(): _getdistance(context); // distance future builder if available
                      },
                      child: Text('Cone 1 & 2',
                        style: TextStyle(
                          fontFamily: 'fira',
                          color: Colors.black,
                          fontSize: 20,
                        ),
                      ),
                    )
                  )
                ],
              ),
              SizedBox(width: 100,), //150),
              Column(
                children: <Widget>[
                  Text(
                    'Distance/cm',
                    style: TextStyle(
                      fontFamily: 'fira',
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 20
                    ),
                  ),
                  SizedBox(height:15), //2
                  Container(
                    color: Colors.lightBlue[100],
                    width: 120, //100,
                    height: 40,  //40
                    padding: EdgeInsets.only(left: 5, top: 8),
                    child: Text(
                      getDistance, // would be updated if toastAlert is false
                      style: TextStyle(
                        fontFamily: 'fira',
                        color: Colors.black,
                        fontStyle: FontStyle.italic,
                        fontSize: 20,
                      ),
                    )
                  )
                ],
              ),
            ],
          ),
          SizedBox(height: 0), //10
          MySlider(),
        ]
      )
    );
  }
}