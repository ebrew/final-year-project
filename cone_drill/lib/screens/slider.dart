import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';


class MySlider extends StatefulWidget{
  @override
  _SliderState createState() => _SliderState();
}

class _SliderState extends State<MySlider>{

  double _value = 0.5; //initial slider value

  // cloud/device connection and slider manipulations
  getSlider(double val) async{
    setState(() {
      _value = val;
    });
    http.Response response = await http.get("http://192.168.4.1/?State=${(_value * 10).round()}");
    return response.body;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top:0),
        child: Center(
          child: Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                child: Slider(
                  value: _value,
                  onChanged: getSlider,  // connection to the the device/cloud
                  divisions: 10,
                  activeColor: Colors.amberAccent[400],
                  inactiveColor: Colors.black38,
                )
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(width: 0), //18
                  Text(
                    'Speed Regulator:',
                    style: TextStyle(
                      fontFamily: 'fira',
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                      fontSize: 18
                    ),
                  ),
                  Container(
                   color: Colors.lightBlue[100],
                   width: 111, //122
                   height: 35, //30
                   margin: EdgeInsets.only(left: 120),
                   padding: EdgeInsets.only(left: 5, top: 5),
                   child: Text(
                     'Speed: ${(_value * 10).round()}',
                     style: TextStyle(
                       fontFamily: 'fira',
                       fontStyle: FontStyle.italic,
                       color: Colors.black,
                       fontSize: 20,
                      ),
                    )
                  )
                ],
              ),
            ],
          ),
        )
    );
  }
}

