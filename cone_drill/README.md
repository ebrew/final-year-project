# cone_drill
A new Flutter  final year project.

This project is developed for footballers to be used purposely for training.
This mobile app is used for the control of the robotic smart cone drills
which have ability to move in any direction set by the user with the help of this mobile. 



## Developer Details
- [Name](Emmanuel Brew)
- [Email](emmanuelbrew85@gmail.com)
- [Institution](KNUST, Ghana)
- [Gitlab Account](https://gitlab.com/ebrew)

## Project members
- Emmanuel Brew
- Richard Tanoh Agyemang
- [Department](Computer Engineering, Ghana)
- [Institution](KNUST, Ghana)
- [Year of Graduation](2020/2021).